const request = require('request');
const headers = {
    'Content-type': 'application/json'
};
const argv = require('yargs').argv;

let message;

if (argv.message) {
  message = argv.message;
}
else {
  message = "Hello world!";
}
const dataString = `{"text": "${message}"}`;
const options = {
    url: 'https://hooks.slack.com/services/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',
    method: 'POST',
    headers: headers,
    body: dataString
};

function callback(error, response, body) {
    if (!error && response.statusCode == 200) {
        console.log(body);
    }
}

request(options, callback);
